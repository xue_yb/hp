package net.hserver.hp.client;

/**
 * @author hxm
 */
public interface CallMsg {

    /**
     * 消息回调状态
     *
     * @param msg
     */
    void message(String msg);

}
